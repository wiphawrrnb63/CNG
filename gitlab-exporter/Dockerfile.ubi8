ARG RUBY_IMAGE=

FROM ${RUBY_IMAGE}

ARG GITLAB_EXPORTER_VERSION
ARG GITLAB_USER=git
ARG UID=1000
ARG DNF_OPTS

LABEL source="https://gitlab.com/gitlab-org/build/CNG/-/tree/master/gitlab-exporter" \
      name="GitLab Exporter" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${GITLAB_EXPORTER_VERSION} \
      release=${GITLAB_EXPORTER_VERSION} \
      summary="Prometheus Web exporter for GitLab." \
      description="Prometheus Web exporter for GitLab."

ENV CONFIG_TEMPLATE_DIRECTORY=/var/opt/gitlab-exporter/templates
ENV CONFIG_DIRECTORY=/etc/gitlab-exporter
ENV CONFIG_FILENAME=gitlab-exporter.yml

ADD gitlab-exporter.tar.gz /

COPY scripts/ /scripts/

RUN dnf ${DNF_OPTS} install -by --nodocs procps uuid \
    && dnf clean all \
    && rm -r /var/cache/dnf \
    && adduser -m ${GITLAB_USER} -u ${UID} \
    && mkdir -p /var/log/gitlab ${CONFIG_DIRECTORY} \
    && chown -R ${UID}:0 /var/log/gitlab ${CONFIG_DIRECTORY} /scripts \
    && chmod -R g=u ${CONFIG_DIRECTORY} /scripts \
    && ldconfig

USER ${UID}

CMD ["/scripts/process-wrapper"]

HEALTHCHECK --interval=30s --timeout=30s --retries=5 CMD /scripts/healthcheck
