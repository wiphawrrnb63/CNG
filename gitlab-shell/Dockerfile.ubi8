ARG RUBY_IMAGE=

FROM ${RUBY_IMAGE}

ARG GITLAB_SHELL_VERSION
ARG GITLAB_USER=git
ARG UID=1000
ARG DNF_OPTS

LABEL source="https://gitlab.com/gitlab-org/build/CNG/-/tree/master/gitlab-shell" \
      name="GitLab Shell" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${GITLAB_SHELL_VERSION} \
      release=${GITLAB_SHELL_VERSION} \
      summary="SSH access and repository management app for GitLab." \
      description="SSH access and repository management app for GitLab."

ADD gitlab-shell.tar.gz /
ADD gitlab-logger.tar.gz /usr/local/bin
ADD gitlab-gomplate.tar.gz /

COPY scripts/ /scripts/
COPY sshd_config /etc/ssh/

RUN dnf ${DNF_OPTS} install -by --nodocs procps fipscheck-lib openssh openssh-server \
    && dnf clean all \
    && rm -r /var/cache/dnf \
    && rm -r /usr/local/tmp /usr/libexec/openssh/ssh-keysign \
    && adduser -m ${GITLAB_USER} -u ${UID} \
    && mkdir -p /srv/sshd /var/log/gitlab-shell \
    && touch /var/log/gitlab-shell/gitlab-shell.log \
    && mv /scripts/authorized_keys /authorized_keys \
    && chown -R ${UID}:0 /srv/sshd /srv/gitlab-shell /var/log/gitlab-shell /etc/ssh /scripts /home/${GITLAB_USER} \
    && chmod -R g=u /srv/sshd /srv/gitlab-shell /var/log/gitlab-shell /etc/ssh /scripts /home/${GITLAB_USER} \
    && chmod 0755 /authorized_keys

ENV CONFIG_TEMPLATE_DIRECTORY=/srv/gitlab-shell

USER ${UID}

CMD ["/scripts/process-wrapper"]

VOLUME /var/log/gitlab-shell

HEALTHCHECK --interval=10s --timeout=3s --retries=3 CMD /scripts/healthcheck
